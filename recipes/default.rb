#followed by the below link
#https://help.ubuntu.com/community/Internet/ConnectionSharing

subnet      = "172.16.33.0/24"
masquerade  = "iptables -t nat -A POSTROUTING -s #{subnet} -o eth0 -j MASQUERADE"

#execute "configure internal network card" do
#  command   "ip addr add #{subnet} dev eth1"
#end

#execute "configure NAT" do
#  command   "iptables -A FORWARD -o eth0 -i eth1 -s #{subnet} -m conntrack --ctstate NEW -j ACCEPT"
#  command   "iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT"
#  command   "iptables -t nat -F POSTROUTING"
#  command   "iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE"
#end

#execute "save & edit iptables" do
#  command   "iptables-save | sudo tee /etc/iptables.sav"
#  command   "iptables-restore < /etc/iptables.sav"
#end

#execute "enable routing" do
#  command   "sh -c \"echo 1 > /proc/sys/net/ipv4/ip_forward\""
#end



execute "Enable IPv4 forwarding in /etc/sysctl.conf" do
  command   "sed -ie 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf"
  not_if    "grep -q '^net.ipv4.ip_forward=1' /etc/sysctl.conf >/dev/null"
end

execute "Enable IPv4 forwarding on running node" do
  command   "sysctl -w net.ipv4.ip_forward=1"
  not_if    "sysctl net.ipv4.ip_forward | grep -q '= 1$' >/dev/null"
end

execute "Add iptables masquerading to /etc/rc.local" do
  command   "sed -ie 's|^\\(exit 0\\)$|#{masquerade}\\n\\n\\1|' /etc/rc.local"
  not_if    "grep -q 'iptables .* MASQUERADE' /etc/rc.local >/dev/null"
end

execute "Enable iptables masquerading on running node" do
  command   masquerade
  not_if    "iptables -L | grep -q '#{subnet}.*ESTABLISHED' >/dev/null"
end
