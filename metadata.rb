name              "router"
maintainer        "Fletcher Nichol"
maintainer_email  "fnichol@nichol.ca"
license           "Apache 2.0"
description       "Quick and dirty NAT router with iptables"
long_description  "Quick and dirty NAT router with iptables"
version           "0.1.3"

supports "ubuntu"

#followed by the below link
#https://help.ubuntu.com/community/Internet/ConnectionSharing